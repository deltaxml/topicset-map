# Topicset Map sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DITA-Compare-8_0_0_j/samples/sample-name`.*

---

## Summary

This sample compares two DITA maps, doc1/maps/main.ditamap and doc2/maps/main.ditamap, using all three of the available map result structures: the 'topic set', the 'map-pair' and the 'unified-map'. For more details see: [Topicset Map documentation](https://docs.deltaxml.com/dita-compare/latest/topicset-map-sample-3801205.html).

## Running the sample via the Ant build script

The sample comparison can be run via an Apache Ant build script using the following commands.

| Command | Actions Performed |
| --- | --- |
| ant run | Run all comparisons. |
| ant run-topic-set-A | Run the 'topic set' comparison (where the first 'A' input is being used as the 'result origin'). |
| ant run-map-pair-B | Run the 'map pair' comparison (where the second 'B' input is being used as the 'result origin'). |
| ant run-unified-map-B | Run the 'unified map' comparison (where the second 'B' input is being used as the 'result origin'). |
| ant run-map-pair-B-otcs | Run the 'map pair' comparison with the oXygen tracked changes output format selected. |
| ant run-unified-map-B-otcs | Run the 'unified map' comparison with the oXygen tracked changes output format selected. |
| ant clean | Remove the generate output. |

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

The sample can be run directly by issuing the following command

    java -jar ../../deltaxml-dita.jar compare map doc1/maps/main.ditamap doc1/maps/main.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set

The 'map pair' or 'unified map' comparisons can be run in a similar manner to the 'topic set', but in this case the map-result-orign is B, the map-result-structure is map-pair or unified-map, and the map-copy-location is map-pair-B or unified-map-B. Further, it is possible to select the oXygen tracked changes output format, by adding the parameter output-format=oxygen-tcs to the arguments.

To clean up the sample directory, run the following Ant command.

	ant clean